# -*- coding: utf-8 -*-
# pylint: disable=undefined-variable


def __init__(hub):
    hub.context["subinit"] = True


def inited(hub):
    return "subinit" in hub.context
