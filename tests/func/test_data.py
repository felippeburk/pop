# -*- coding: utf-8 -*-

import pop.exc
import pop.hub
import pytest


@pytest.fixture(scope="session")
def hub():
    hub = pop.hub.Hub()
    yield hub


def test_imap(hub):
    map = hub.pop.data.imap({})
    assert isinstance(map, hub.pop.data.IMAP)


def test_map(hub):
    map = hub.pop.data.map()
    assert isinstance(map, hub.pop.data.MAP)


def test_omap(hub):
    map = hub.pop.data.omap()
    assert isinstance(map, hub.pop.data.OwnerWriteableMapping)
